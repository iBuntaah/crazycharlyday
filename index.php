<?php

use Illuminate\Database\Capsule\Manager as DB;
require_once ('vendor/autoload.php');




//exemple de creation de modele
//$panier = new \giftbox\modele\Panier();

$app = new \Slim\Slim();
// Connexion
try {
	$tab = parse_ini_file('src/conf/conf.ini');
	$db = new DB();
	$db->addConnection($tab);
	$db->setAsGlobal();
	$db->bootEloquent();
} catch (PDOException $e) {
	echo $e;
}


$app->get('/logement/places/:id', 'crazyday\controleur\ControleurCatalogue:afficherLogementDispoPlace');

$app->get('/logement/placesPlus', 'crazyday\controleur\ControleurCatalogue:afficherLogementDispoPlaceSup');

$app->get('/logementDisponible', 'crazyday\controleur\ControleurCatalogue:afficherLogementDispo');

$app->get('/logementIndisponible', 'crazyday\controleur\ControleurCatalogue:afficherLogementIndispo');




// Detail d'un logement
$app->get('/logement/:id', 'crazyday\controleur\ControleurCatalogue:detailLogement');

//details utilisateur
$app->get('/user/:id', 'crazyday\controleur\ControleurCatalogue:afficherDetailsUser');

//details groupe
//$app->get('/user/:id', 'crazyday\controleur\ControleurAffichage:detailGroupe');

//login
$app->post('/login', 'giftbox\controleur\ControleurAdmin:login');

$app->get('/', 'crazyday\controleur\ControleurIndex:index');

$app->get('/membre', 'crazyday\controleur\ControleurCatalogue:afficherMembres');

$app->get('/connexion', 'crazyday\controleur\ControleurConnexion:afficherConnexion');


//connexion
$app->post('/authentification', 'crazyday\controleur\ControleurConnexion:seConnecter');



$app->get('/user/:id', 'crazyday\controleur\ControleurCatalogue:afficherDetailsUser');


$app->get('/creergroupe', 'crazyday\controleur\ControleurGroupe:creerGroupe');


$app->get('/inscrire', 'crazyday\controleur\ControleurConnexion:inscrire');


$app->run();
?>