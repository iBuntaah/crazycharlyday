<?php
namespace crazyday\controleur;
use crazyday\modele\Logement as Logement;
use crazyday\modele\Groupe as Groupe;
use crazyday\modele\User as User;
use crazyday\modele\Invitation as Invitation;
use crazyday\vue\VueCatalogue as VueCatalogue;
use crazyday\vue\VueNavbar as VueNavbar;
use crazyday\vue\VueFooter as VueFooter;
use crazyday\vue\VueDetaillee as VueDetaillee;


class ControleurCatalogue
{
    function afficherLogementDispo() {
        $listeLogement = Logement::where('places', '>', 0)->get();
        $vueCatalogue = new VueCatalogue($listeLogement);
        $vueNavbar=new VueNavbar('../');

        $vueFooter=new VueFooter('../');
        $html=$vueNavbar->render().$vueCatalogue->render(1).$vueFooter->render();
        echo $html;
    }

    function afficherLogementDispoPlace($nb) {
        $listeLogement = Logement::where('places', '=', $nb)->get();
        $vueCatalogue = new VueCatalogue($listeLogement);
        $vueNavbar=new VueNavbar('../');

        $vueFooter=new VueFooter('../');
        $html=$vueNavbar->render().$vueCatalogue->render(1).$vueFooter->render();
        echo $html;
    }

    function afficherLogementIndispo() {
        $listeLogement = Logement::where('places', '==', 0)->get();
        $vueCatalogue = new VueCatalogue($listeLogement);
        $vueNavbar=new VueNavbar('../');

        $vueFooter=new VueFooter('../');
        $html=$vueNavbar->render().$vueCatalogue->render(1).$vueFooter->render();
        echo $html;
    }

    function afficherLogementDispoPlaceSup() {
        $listeLogement = Logement::where('places', '>', 5)->get();
        $vueCatalogue = new VueCatalogue($listeLogement);
        $vueNavbar=new VueNavbar('../');

        $vueFooter=new VueFooter('../');
        $html=$vueNavbar->render().$vueCatalogue->render(1).$vueFooter->render();
        echo $html;
    }

    public function detailLogement($idLogement){
        $logement = Logement::where("id", "=", $idLogement)->first();
        $v = new VueDetaillee($logement);
        $vueNavbar=new VueNavbar('../');
        $vueFooter=new VueFooter('../');
        $html=$vueNavbar->render().$v->render(1).$vueFooter->render();
        echo $html;
    }

    public function afficherDetailsUser($idUser)
    {
        $user = User::where("id", "=", $idUser)->first();
        $v = new VueDetaillee($user);
        $vueNavbar=new VueNavbar('../');
        $vueFooter=new VueFooter('../');
        $html=$vueNavbar->render().$v->render(2).$vueFooter->render();
        echo $html;
    }

    function afficherMembres() {

        $listeMembre = User::get();
        $vueCatalogue = new VueCatalogue($listeMembre);
        $vueNavbar=new VueNavbar('../');

        $vueFooter=new VueFooter('../');
        $html=$vueNavbar->render().$vueCatalogue->render(2).$vueFooter->render();
        echo $html;
    }
}