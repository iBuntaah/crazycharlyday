<?php
namespace crazyday\controleur;
use crazyday\vue\VueNavbar as VueNavbar;
use crazyday\vue\VueIndex as VueIndex;
use crazyday\vue\VueFooter as VueFooter;


class ControleurIndex {

    public function index(){
        $v1=new VueNavbar('../');
        $v2=new VueIndex('../');
        $v3=new VueFooter('../');
        $html=$v1->render().$v2->render().$v3->render();
        echo $html;
    }

}