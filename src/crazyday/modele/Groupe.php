<?php
/**
 * Created by PhpStorm.
 * User: Yawen
 * Date: 09/02/2017
 * Time: 11:25
 */

namespace crazyday\modele;


class Groupe extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'groupe';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function Logement(){
        return $this->hasOne('crazyday\modele\Logement', 'id_loge');
    }

    public function User(){
        return $this->hasMany('crazyday\modele\User', 'id');
    }
}