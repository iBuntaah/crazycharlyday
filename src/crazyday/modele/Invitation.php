<?php
/**
 * Created by PhpStorm.
 * User: Yawen
 * Date: 09/02/2017
 * Time: 12:08
 */

namespace crazyday\modele;


class Invitation extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'invitation';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function Groupe(){
        return $this->belongsTo('crazyday\modele\Logement', 'id_loge');
    }

    public function User(){
        return $this->hasMany('crazyday\modele\User', 'id');
    }
}