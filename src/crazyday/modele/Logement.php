<?php
namespace crazyday\modele;

class Logement extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'logement';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function Groupe(){
        return $this->hasMany('giftbox\modele\Groupe', 'id_loge');
    }
}