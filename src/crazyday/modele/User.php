<?php
namespace crazyday\modele;

class User extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function Logement(){
        return $this->belongsTo('crazyday\modele\Logement', 'id_loge');
    }

    public function Groupe(){
        return $this->belongsTo('crazyday\modele\Groupe', 'id_groupe');
    }
}