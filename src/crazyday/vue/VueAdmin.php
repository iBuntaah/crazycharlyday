<?php
/**
 * Created by PhpStorm.
 * User: samy
 * Date: 09/02/2017
 * Time: 15:57
 */
namespace crazyday\vue;
class VueAdmin
{
    private $route;
    public function __construct()
    {
        $this->route=\Slim\Slim::getInstance()->request()->getRootUri();
    }

    public function render(){
        $content='<div class="wrapper row3">
      <main class="hoc container clear"> 
        <div class="content"> 
          <h1>Ajouter un nouveau logement</h1>
          <form>
            Décrivez votre logement : <input type="text"><br>   
            Nombre de place : <input type="number"><br> 
            Adresse : <input type="text"><br>  
            <input type="submit" value="Créer un nouveau logement">
          </form><br>
          <h1>Information sur les logements et les groupes associés</h1>
          <div class="scrollable">
            <table>
              <thead>
                <tr>
                  <th>Logement</th>
                  <th>Groupe</th>

                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><a href="#">NOMLogement</a></td>
                  <td>Nom groupe</td>
                </tr>
                <tr>
                 <td><a href="#">NOMLogement</a></td>
                 <td>Nom groupe</td>
               </tr>
               <tr>
                <td><a href="#">NOMLogement</a></td>
                <td>Nom groupe</td>
              </tr>
            </tbody>
          </table>
        </div><br>
        <h1>Libérer une place à un logement</h1><form>
        Choisissez le logement dont la place s\'est libérée<br>  
        <select id="ajout" name="ajout">
          <option>Logement 1</option>  
          <option>Logement 2</option>  
        </select>
        <input type="submit" value="Libérer une place">
      </form><br>
      <h1>Bloquer une place à un logement</h1><form>
      Choisissez le logement dont la place va être bloqué<br>  
      <select id="ajout" name="ajout">
        <option>Logement 1</option>  
        <option>Logement 2</option>  
      </select>
      <input type="submit" value="Bloquer une place">
    </form><br>
    <h1>Groupes en attente d\'un logement</h1><form>
    Choisissez l\'action à faire<br>  
    <select id="ajout" name="ajout">
      <option>Groupe 1 Logement 1</option>  
      <option>Groupe 2 Logement 2</option>  
    </select>
    <input type="submit" value="Accepter groupe"> <input type="submit" value="Refuser groupe">
  </form>
  <br>
</div>
</div>';

        return $content;
    }

}
