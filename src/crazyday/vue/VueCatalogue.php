<?php

/**
 * Created by PhpStorm.
 * User: samy
 * Date: 09/02/2017
 * Time: 11:11
 */
namespace crazyday\vue;

class VueCatalogue
{
    private $route;
    protected $liste;
    public function __construct($l){
        $this->route=\Slim\Slim::getInstance()->request()->getRootUri();
        $this->liste = $l;
    }
    public function render($id)
    {
        switch($id){
            case 1:
                $content='<div class="wrapper row3">
    <main class="hoc container clear"> 
      <div class="content"> 
        <div id="gallery">
          <figure>
            <header class="heading">Liste des logements</header>
            <ul class="nospace clear">';
                $i = 0;

                foreach( $this->liste as $logement){
                    $tmp = " first";
                    if($i % 4 != 0){
                        $tmp = "";

                    }else{
                        $i = 0;
                    }
                    $i++;
                    $content = $content . '
            <li class="one_quarter'.$tmp  .'"><a href="'.$this->route.'/logement/'. $logement->id.'"><img src="'.$this->route. '/../web/images/demo/apart/'.$logement->img .'" alt=""></a><figcaption>Logement numéro: '. $logement->id . '</figcaption>
                <figcaption>'.$logement->adresse .'</figcaption>
                <figcaption><a href"#">En savoir plus</a></figcaption>
              </li>';
                }
                $content = $content . '
    <!-- REPETER ÇA  BELEK AU FIRST POUR REVENIR À LA COLONNE-->
            </ul>
          </figure>
        </div>
      </div>
      <div class=\"clear\"></div>
    </main>
  </div>';

                break;
            case 2:
                $content='<div class="wrapper row3">
    <main class="hoc container clear"> 
      <div class="content"> 
        <div id="gallery">
          <figure>
            <header class="heading">Liste des membres</header>
            <ul class="nospace clear">';
                $i = 0;

                foreach( $this->liste as $logement){
                    $tmp = " first";
                    if($i % 4 != 0){
                        $tmp = "";

                    }else{
                        $i = 0;
                    }
                    $i++;
                    $content = $content . '
            <li class="one_quarter'.$tmp .'"><a href="'.$this->route.'/user/'. $logement->id.'"><img src="'.$this->route. '/../web/images/demo/user/'.$logement->img .'" alt=""></a><figcaption>'. $logement->nom . '</figcaption>
                <figcaption>'.$logement->adresse .'</figcaption>
                <figcaption><a href"#">En savoir plus</a></figcaption>
              </li>';
                }
                $content = $content . '
    <!-- REPETER ÇA  BELEK AU FIRST POUR REVENIR À LA COLONNE-->
            </ul>
          </figure>
        </div>
      </div>
      <div class=\"clear\"></div>
    </main>
  </div>';
break;
        }

        return $content;

    }
}