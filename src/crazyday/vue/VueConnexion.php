<?php
/**
 * Created by PhpStorm.
 * User: samy
 * Date: 09/02/2017
 * Time: 16:02
 */

namespace crazyday\vue;
class VueConnexion
{
    private $route;
    public function __construct()
    {
        $this->route=\Slim\Slim::getInstance()->request()->getRootUri();
    }

    public function render(){
        $content='<div class="wrapper bgded overlay" style="background-image:url('.$this->route.'/web/images/demo/backgrounds/connexion.jpg);">
  <div class="hoc container clear center"> 
    <article>
      <h2 class="heading">Espace de connexion</h2>
      <p class="nospace btmspace-50 font-x1">Connectez vous pour rejoindre votre espace membre ou administrateur</p>
      <footer>
        <ul class="nospace inline pushright"><form action = "'.$this->route.'/authentification" method = "post">
       <li>Nom d\'utilisateur<input type="text" name = "user"></li>
        <li>Mot de passe<input type="password" name = "password"></li>
      </ul>
        <ul class="nospace inline pushright"><br>
          <li><input type="submit" value="Me Connecter" style="height:200px; width:200px"></form></li>
        </ul>
      </footer>
    </article>
  </div>
</div>';

        return $content;

}
}