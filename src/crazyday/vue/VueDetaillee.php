<?php
/**
 * Created by PhpStorm.
 * User: samy
 * Date: 09/02/2017
 * Time: 15:55
 */

namespace crazyday\vue;
class VueDetaillee
{
    private $route;
    private $element;
    public function __construct($element)
    {
        $this->route=\Slim\Slim::getInstance()->request()->getRootUri();
        $this->element = $element;
    }

    public function render($id){

        switch($id){
            case 1:
                $content='<div class="wrapper row3">
  <main class="hoc container clear"> 
    <div class="content three_quarter first"> 
      <h1>Appartement numéro ' . $this->element->id .'</h1>
      <img class="imgr borderedbox inspace-5" src="'.$this->route. '/../web/images/demo/apart/'.$this->element->img .'" alt="">
      <p>' . $this->element->description .'</p>
     <a class="btn" href="#">Rejoindre cet appartement</a>
    </div>
    <div class="sidebar one_quarter"> 
      <div class="sdb_holder">
        <h6>Les coordonnées</h6>
        <address>
        Adresse du lieu: ' . $this->element->adresse .'<br>
        Nombre de places: ' . $this->element->places .'<br>
        Prix par personne: ' . $this->element->prix .' euros<br>
        <br>
        Contact: crazycharlyday@gmail.com<br>
        </address>
      </div>
    </div>
    <div class="clear"></div>
  </main>
</div>';
break;
            case 2:
                $content='<div class="wrapper row3">
  <main class="hoc container clear"> 
    <div class="content three_quarter first"> 
      <h1>On vous présente ' . $this->element->nom .'</h1>
      <img class="imgr borderedbox inspace-5" src="'.$this->route. '/../web/images/demo/user/'.$this->element->img .'" alt="">
      <p>'.$this->element->nom.' ' . $this->element->message .'</p>
     <a class="btn" href="#">Ajouter à mon groupe</a>
    </div>
    <div class="sidebar one_quarter"> 
      <div class="sdb_holder">
        <h6>Les coordonnées</h6>
        <address>
        Nom: ' . $this->element->nom .'<br>
        Type d\'utilisateur: ' . $this->element->type .'<br>
        Numéro de groupe actuel: ' . $this->element->id_groupe .' euros<br>
        <br>
        Contact: crazycharlyday@gmail.com<br>
        </address>
      </div>
    </div>
    <div class="clear"></div>
  </main>
</div>';
break;
        }

        return $content;
    }

}
