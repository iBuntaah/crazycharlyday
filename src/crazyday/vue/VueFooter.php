<?php

/**
 * Created by PhpStorm.
 * User: samy
 * Date: 09/02/2017
 * Time: 11:15
 */

namespace crazyday\vue;
class vueFooter
{
    private $route, $problemeLien;
    public function __construct($line = '')
    {
        $this->route=\Slim\Slim::getInstance()->request()->getRootUri();
        $this->problemeLien = $line;
    }

    public function render(){

        $conte='<div class="wrapper row4">
  <footer id="footer" class="hoc clear"> 
    <div class="one_quarter first">
      <h6 class="title">À propos</h6>
      <p>Nous avons réalisé ce site internet dans le but de notre projet Crazy Charly Day.</p>
      <p>Tout ce qui est présent sur ce site est fictif.</p>
    </div>
    <div class="one_quarter">
      <h6 class="title">Adresse et contact</h6>
      <ul class="nospace linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
          IUT Nancy Charlemagne
          </address>
        </li>
        <li><i class="fa fa-phone"></i>0312345678</li>
        <li><i class="fa fa-envelope-o"></i>charlyday@gmail.com</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">Accéder aux fonctionnalités</h6>
      <ul class="nospace linklist">
        <li><a href="#">Logements</a></li>
        <li><a href="#">Membres</a></li>
        <li><a href="'.$this->route. '/' . $this->problemeLien . '/index.php/connexion"">Connexion</a></li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">Notre groupe</h6>
        <li>
          Nael Bouzaza SI1<br>
        </li>
        <li>
          Tolga Gurel AI1<br>
        </li>
        <li>
          Yawen Daba AI1<br>
        </li>
        <li>
          Samy Elayachi SI2<br>
        </li>
    </div>
  </footer>
</div>
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<script src="'.$this->route. '/' . $this->problemeLien . 'web/layout/scripts/jquery.min.js"></script>
<script src="'.$this->route. '/' . $this->problemeLien . 'web/layout/scripts/jquery.backtotop.js"></script>
<script src="'.$this->route. '/' . $this->problemeLien . 'web/layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>';

        return $conte;

    }


}
