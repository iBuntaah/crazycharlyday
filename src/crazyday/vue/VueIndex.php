<?php
/**
 * Created by PhpStorm.
 * User: ToLgA
 * Date: 09/02/2017
 * Time: 15:29
 */

namespace crazyday\vue;


class VueIndex
{

    private $route, $problemeLien;

    public function __construct($line = '')
    {
        $this->route = \Slim\Slim::getInstance()->request()->getRootUri();
        $this->problemeLien = $line;
    }

    public function render()
    {
        $content = '<div id="pageintro" class="hoc clear"> 
    <article class="introtxt">
      <p>La 1ère application de collocation en ligne sur Nancy<br><i> (en partenariat avec "Un Toit Pour Tous")</i></p>
      <h2 class="heading">Trouvez vous aussi la collocation de vos rêves !</h2>
      <footer><a class="btn" href="'.$this->route.'/logementDisponible">Accéder à la liste de toutes les collocations</a></footer>
    </article>
    <div class="clear"></div>
  </div>
</div>
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <div class="group center">
      <article class="one_quarter first"><i class="icon fa fa-check"></i>
        <h4 class="font-x1"><a href="#">Simplicité</a></h4>
        <p>Trouver une collocation de qualité n\'a jamais été aussi simple !</p>
      </article>
      <article class="one_quarter"><i class="icon fa fa-users"></i>
        <h4 class="font-x1"><a href="#">Rencontres</a></h4>
        <p>Rencontrez des personnes qui vous correspondent ou à l\'inverse totalement différentes pour vivre une experience inoubliable</p>
      </article>
      <article class="one_quarter"><i class="icon fa fa-clock-o"></i>
        <h4 class="font-x1"><a href="#">Rapidité</a></h4>
        <p>Notre service technique vous informe au plus vite de l\'acceptation de votre dossier</p>
      </article>
      <article class="one_quarter"><i class="icon fa fa-thumbs-o-up "></i>
        <h4 class="font-x1"><a href="#">Confiance</a></h4>
        <p>Bénéficiez d\'un système d\'avis vous permettant de vous assurer de la qualité de vos futurs collocataires</p>
      </article>
    </div>
    <div class="clear"></div>
  </main>
</div>
<div class="wrapper bgded overlay" style="background-image:url('.$this->route. '/' . $this->problemeLien . '/web/images/demo/backgrounds/membre.jpg);">
  <div class="hoc container clear center"> 
    <article>
      <h2 class="heading">Vous êtes membres ?</h2>
      <p class="nospace btmspace-50 font-x1">Vous faites donc partis d\'une communauté de plus de X utilisateurs ! Vous bénéficiez des dernières informations sur nos logements et vous pouvez à tout moment créer un groupe pour une collocation !</p>
      <footer>
        <ul class="nospace inline pushright">
          <li><a class="btn" href="'.$this->route. '/' . $this->problemeLien . '/index.php/connexion">Connectez vous !</a></li>
          <li><a class="btn inverse" href="'.$this->route. '/' . $this->problemeLien . '/index.php/inscrire">Vous n\'êtes toujours pas membre? Inscrivez vous !</a></li>
        </ul>
      </footer>
    </article>
  </div>
</div>
<div class="wrapper">
  <div class="split clear" style="background-image:url('.$this->route.'/' . $this->problemeLien . '/web/images/demo/backgrounds/logements.jpg);">
    <section class="box"> 
      <div class="btmspace-50">
        <h3 class="font-x2 nospace">Nos logements</h3>
        <p class="nospace">Tous nos logements sont sélectionnés selon des critères chers à notre entreprise.</p>
      </div>
      <ul class="nospace group services">
        <li class="one_half first">
          <article><a href="#"><i class="fa fa-globe"></i></a>
            <h6 class="heading">Eco-Financement</h6>
            <p>Nos logements ont tous été financés via des Eco-Prêts, nous voulons apporter notre pierre à l\'édifice du développement durable et éco responsable.</p>
          </article>
        </li>
        <li class="one_half">
          <article><a href="#"><i class="fa fa-building"></i></a>
            <h6 class="heading">Batiments inter-générationnels</h6>
            <p>Afin de participer aux échanges inter-générationnels, nos batiments sont tous composés de résidants de différentes classes d\'âge. De beaux moments de partage sont à envisager !</p>
          </article>
        </li>
        <li class="one_half first">
          <article><a href="#"><i class="fa fa-book"></i></a>
            <h6 class="heading">Logements réhabilités selon les dernières normes</h6>
            <p>Tous nos logements sont aux normes françaises et européenne. La sécurité et le bien être de nos habitants font partis de maîtres mot.</p>
          </article>
        </li>
        <li class="one_half">
          <article><a href="#"><i class="fa fa-eye"></i></a>
            <h6 class="heading">Voisinage vigilants</h6>
            <p>Tous nos voisins sont membres de la communauté <a href="https://www.voisinsvigilants.org" target="_blank">Voisins vigilants</a>, ce qui permet de vivre dans un quartier en sécurité</p>
          </article>
        </li>
      </ul>
    </section>
  </div>
</div>
<div class="wrapper row3">
  <section class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <div class="center btmspace-50">
      <h3 class="font-x2 nospace">Nos derniers logements pourvus !</h3>
      <p class="nospace">Découvrez les derniers logements qui ont été pourvus.</p>
    </div>
    <div class="group">
      <article class="one_third first"><img class="btmspace-30" src="'.$this->route. '/' . $this->problemeLien . '/web/images/demo/apart/4.jpg" alt="">
      </article>
      <article class="one_third"><img class="btmspace-30" src="'.$this->route. '/' . $this->problemeLien . '/web/images/demo/apart/2.jpg" alt="">
      </article>
      <article class="one_third"><img class="btmspace-30" src="'.$this->route.'/' . $this->problemeLien . '/web/images/demo/apart/3.jpg" alt="">
      </article>
    </div>
  </section>
</div>';
        return $content;

    }

}