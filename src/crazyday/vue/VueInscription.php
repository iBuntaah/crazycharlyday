<?php
/**
 * Created by PhpStorm.
 * User: samy
 * Date: 09/02/2017
 * Time: 17:48
 */

namespace crazyday\vue;


class VueInscription
{

    private $route;

    public function __construct()
    {
        $this->route = \Slim\Slim::getInstance()->request()->getRootUri();
    }

    public function render()
    {
        $content = '<div class="wrapper bgded overlay" style="background-image:url('.$this->route.'/web/images/demo/backgrounds/connexion.jpg\');">
  <div class="hoc container clear center"> 
    <article>
      <h2 class="heading">Espace d\'inscription</h2>
      <p class="nospace btmspace-50 font-x1">Inscrivez vous pour rejoindre la communauté de La Coloc En Ligne</p>
      <footer>
        <ul class="nospace inline pushright">
       <li>Nom d\'utilisateur<input type="text"></li>
        <li>Mot de passe<input type="password"></li>
        <li>Une petite description de vous<input type="text"></li>
      </ul>
        <ul class="nospace inline pushright"><br>
          <li><input type="submit" value="Inscription" style="height:200px; width:200px"></li>
        </ul>
      </footer>
    </article>
  </div>
</div>';
        return $content;

    }
}