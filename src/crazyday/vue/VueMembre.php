<?php
/**
 * Created by PhpStorm.
 * User: nael
 * Date: 09/02/2017
 * Time: 15:54
 */

namespace crazyday\vue;
class VueMembre
{
    private $route;
    public function __construct()
    {
        $this->route=\Slim\Slim::getInstance()->request()->getRootUri();
    }

    public function affich_log_deroulant(){
        $html="";
        $presta =Logement::get();
        foreach ($presta as $value) {
            if($value['places']!=0){
                $html=$html."<option>".$value['nom']."</option>";
            }
        }
        return $html;
    }

    public function affich_cat_deroulant(){
        $html="";
        $presta =User::get();
        foreach ($presta as $value) {
            if($value['id_groupe']==0){
                $html=$html."<option>".$value['nom']."</option>";
            }
        }
        return $html;
    }

    public function render()
    {
        $content = '<div class="wrapper row3">
  <main class="hoc container clear"> 
    <div class="content"> 
      <h1>Créer un nouveau groupe</h1>
      <form>
      Décrivez votre groupe : <input type="text"><br>    
      <input type="submit" value="Créer un nouveau groupe">
    </form><br>
      <h1>Informations sur mon groupe</h1>
      Numéro de mon groupe:<br><br>
      État:<br><br>
      Logement associé:<br><br>
      <div class="scrollable">
        <table>
          <thead>
            <tr>
              <th>Membre</th>
              <th>État</th>

            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#">NOM</a></td>
              <td>En attente</td>
            </tr>
            <tr>
              <td><a href="#">NOM</a></td>
              <td>Accepté</td>
            </tr>
            <tr>
              <td><a href="#">NOM</a></td>
              <td>Refusé</td>
            </tr>
          </tbody>
        </table>
      </div><br>
      <h1>Ajouter un membre au groupe</h1><form>
      Choisissez le membre à inviter<br>  
      <select id="ajout" name="ajout">
      '.$this->affich_cat_deroulant().'
    </select>
      <input type="submit" value="Ajouter un membre">
    </form><br>
     <h1>Ajouter un logement au groupe</h1><form>
      Choisissez le logement à ajouter<br>  
      <select id="ajout" name="ajout">
      '.$this->affich_log_deroulant().'
    </select>
      <input type="submit" value="Ajouter un logement">
    </form><br>
    <h1>Ajouter un logement compatible au groupe actuel</h1><form>
      Choisissez le logement à ajouter<br>  
      <select id="ajout" name="ajout">
      <option>Logement 1</option>  
      <option>Logement 2</option>  
    </select>
      <input type="submit" value="Ajouter un logement">
    </form><br>
    <h1>Supprimer un membre au groupe</h1><form>
      Choisissez le membre à supprimer<br>  
      <select id="ajout" name="ajout">
      <option>Membre 1</option>  
      <option>Membre 2</option>  
    </select>
      <input type="submit" value="Supprimer un membre">
    </form>
  <br>
     <h1>Supprimer le logement du groupe</h1><form>
      Choisissez le logement à supprimer<br>  
      <select id="ajout" name="ajout">
      <option>Logement 1</option>  
      <option>Logement 2</option>  
    </select>
      <input type="submit" value="Supprimer un logement">
    </form>
    <div class="clear"></div>
  </main>
</div>';
        return $content;
    }
}