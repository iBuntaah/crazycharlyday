<?php

/**
 * Created by PhpStorm.
 * User: samy
 * Date: 09/02/2017
 * Time: 11:41
 */

namespace crazyday\vue;

class vueNavbar
{
    private $route, $problemeLien;
    public function __construct($line ='')
    {
        $this->route=\Slim\Slim::getInstance()->request()->getRootUri();
        $this->problemeLien = $line;
    }

    public function render(){
        $content='<!DOCTYPE html>
<html>
<head>
<title>La Coloc\' En Ligne</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="'. $this->route. '/'. $this->problemeLien. '/web/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<div class="bgded overlay" style="background-image:url('.$this->route . '/'. $this->problemeLien.'/web/images/demo/backgrounds/logement.jpg);">
  <div class="wrapper row0">
    <div id="topbar" class="hoc clear"> 
      <div class="fl_left">
        <ul class="nospace">
          <li><a href="#"><i class="fa fa-lg fa-home"></i></a></li>
          <li><a href="'.$this->route.'/logementDisponible">Nos logements</a></li>
          <li><a href="'.$this->route.'/connexion">Connexion</a></li>
          <li><a href="'.$this->route.'/inscrire">S\'inscrire</a></li>
        </ul>
      </div>
      <div class="fl_right">
        <ul class="nospace">
          <li><i class="fa fa-phone"></i>0312345678</li>
          <li><i class="fa fa-envelope-o"></i>crazycharlyday@gmail.com</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrapper row1">
    <header id="header" class="hoc clear"> 
      <div id="logo" class="fl_left">
        <h1><a href="'.$this->route.'/">La Coloc\' En Ligne</a></h1>
      </div>
      <nav id="mainav" class="fl_right">
        <ul class="clear">
          <li class="active"><a href="'.$this->route.'/">Accueil</a></li>
          <li><a class="drop" href="'.$this->route.'/logementDisponible">Nos logements disponibles</a>
            <ul>
              <li><a href="'.$this->route.'/logement/places/2">Pour 2 personnes</a></li>
              <li><a href="'.$this->route.'/logement/places/3">Pour 3 personnes</a></li>
              <li><a href="'.$this->route.'/logement/places/4">Pour 4 personnes</a></li>
              <li><a href="'.$this->route.'/logement/placesPlus">Pour 5 personnes et plus</a></li>
            </ul>
          </li>
          <li><a href="'.$this->route.'/membre">Nos membres</a>
          </li>
          <li><a href="'.$this->route.'/logementIndisponible">Les logements déjà pourvus</a></li>
        </ul>
      </nav>
    </header>
  </div>';
        return $content;
    }

}